//
//  JSONDecoding.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation

struct Deal: Decodable {
    var slug: String?
    var name: String?
    var percentOff: String?
    var imageUrl: String?
    var timeCreated: String?
    var expiryDate: String?
    var isExpired: Bool?
    var restaurant: RestaurantDetail
}

struct Restaurant: Decodable {
    var slug: String?
    var name: String?
    var imageUrl: String?
}

struct DealData: Decodable {
    var data: [Deal]
    var page: Int?
    var size: Int?
    var count: Int?
    var status: Int?
    var isSuccess: Bool?
}

struct DealDetail: Decodable {
    var description: String?
    var discountPrice: String?
    var valuePrice: String?
    var imageUrl: String?
    var slug: String?
    var name: String?
    var percentOff: String?
    var expiryDate: String?
    var isExpired: Bool?
    var restaurant: RestaurantDetail
}

struct RestaurantDetail: Decodable {
    var email: String?
    var phoneNumber: String?
    var subAccountCode: String?
    var city: City
    var slug: String?
    var name: String?
    var imageUrl: String?
}

struct City: Decodable {
    var slug: String?
    var name: String?
}

struct DealDetailData: Decodable {
    var data: DealDetail
    var status: Int?
    var isSuccess: Bool?
}

struct RestaurantDetailData: Decodable {
    var data: RestaurantDetail
    var status: Int?
    var isSuccess: Bool?
}

struct Cities: Decodable {
    var deals: [Deal]
    var slug: String?
    var name: String?
}

struct CitiesData: Decodable {
    var data: [Cities]
    var page: Int?
    var size: Int?
    var count: Int?
    var status: Int?
    var isSuccess: Bool?
}

struct Banks: Decodable {
    var name: String?
    var slug: String?
    var code: String?
    var longcode: String?
    var gateway: String?
    var payWithBank: Bool?
    var active: Bool?
    var isDeleted: JSONNull?
    var country: String?
    var currency: String?
    var type: String?
    var id: Int?
    var createdAt: String?
    var updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case name, slug, code, longcode, gateway, active, country, currency, type, id, createdAt, updatedAt
        case payWithBank = "pay_with_bank"
        case isDeleted = "is_deleted"
    }
}

struct BanksData: Decodable {
    var status: Bool?
    var message: String?
    var data: [Banks]
}

struct User: Decodable {
    var restaurant: Restaurant
    var id: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var phoneNumber: String?
    var profilePictureUrl: String?
}

struct UserData: Decodable {
    var data: User
    var status: Int?
    var isSuccess: Bool?
}

class JSONNull: Codable, Hashable {
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public func hash(into hasher: inout Hasher) {}
}
