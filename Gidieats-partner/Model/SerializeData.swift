//
//  SerializeData.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/16/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation
import UIKit

//Serializing data from backend
public func serializeResponse(data: Data) -> [String: Any] {
    do {
        let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        return result ?? [:]
    } catch _ {
        return [:]
    }
}
