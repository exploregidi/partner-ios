//
//  GlobalConstants.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

//Refer here for all constants, including strings, colors, and API constants
struct Constants {
    static let textFieldBorder: CGFloat = 1.0
    static let buttonFont: CGFloat = 16.0
    static let labelFont: CGFloat = 32.0
    static let appLoadingTime: CGFloat = 1.5
    static let topConstraintForTextField: CGFloat = 24.0
    static let leadingConstraintForTextField: CGFloat = 32.0
    static let texFieldHeightOffset: CGFloat = 15.0
    static let trailingConstraintForTextField: CGFloat = 32.0
    static let fontForTextField: CGFloat = 18.0
    static let topConstraintForButton: CGFloat = 12.0
    static let topConstraintBetweenLabels: CGFloat = 16.0
    static let trailingConstraintForLabel: CGFloat = 16.0
    static let spacingBetweenTextFields: CGFloat = 8.0
    static let textFieldHeight: CGFloat = 40.0
    static let buttonCornerRadius: CGFloat = 10.0
    static let buttonToFieldSpacing: CGFloat = 8.0
    static let indicatorValue: CGFloat = 5.0
    static let smallButtonSize: CGFloat = 50.0
    static let indicatorWidth: CGFloat = 50.0
    static let indicatorHeight: CGFloat = 50.0
    static let minPassword: Int = 7
    static let frameInset: CGFloat = 40.0
    static let smallerButtonFont: CGFloat = 12.0
    static let regularLabelFont: CGFloat = 16.0
    static let buttonSideConstraint: CGFloat = 16.0
    static let standardBottomConstant: CGFloat = -14.0
    static let biggerLabelFont: CGFloat = 22.0
    static let midLabelFont: CGFloat = 14.0
    static let animationTiming: Double = 10.0
    static let defaultTime: Double = 5
    static let regularSideConstraint: CGFloat = 16.0
    static let bottomFromViewConstraint: CGFloat = -100.0
    static let buttonFormWidth: CGFloat = 75.0
    static let normalLabelFont: CGFloat = 18.0
    static let collectionCellHeight: CGFloat = 250.0
    static let collectionCellHeightForPad: CGFloat = 500.0
    static let textFielsSpacing: CGFloat = 2.0
    static let buttonDivider: CGFloat = 4.0
    static let pickerViewDivider: CGFloat = 2.5
    static let textFieldWidth: CGFloat = 150.0
    static let labelBackgroundHeight: CGFloat = 60.0
    static let labelNumberOfLines: Int = 2
    static let sideConstraintForLabel: CGFloat = 24.0
    static let defaultLabelLine: Int = 0
    static let bodyLabelFont: CGFloat = 16
    static let dynamicHeight: CGFloat = 0
    static let dropDownTrail: CGFloat = 4.0
    static let pictureHeight: CGFloat = 300.0
    static let pictureHeightForPad: CGFloat = 600.0
    static let imageAlpha: CGFloat = 0.25
    static let defaultItems: Int = 20
    static let scrollCornerRadius: CGFloat = 25.0
    static let minConstraintOffset: CGFloat = 4.0
    static let indexPathDivider: CGFloat = 2.0
    static let viewContentMinDivider: CGFloat = 3.0
    static let percentCharge: Float = 9.09
    static let scrollViewHeight: CGFloat = 585.0
    static let topScrollOffset: CGFloat = 20.0
    static let notchHeightOffset: CGFloat = 44.0
    static let labelBorderWidth: CGFloat = 150.0
    static let labelBorderHeight: CGFloat = 30.0
    static let labelBackgroundAlpha: CGFloat = 0.70
    static let imageBorder: CGFloat = 0.25
    static let accountImageCornerRadius: CGFloat = 45.0
    static let imageWidth: CGFloat = 100.0
    static let imageHeight: CGFloat = 100.0
    static let tableHeightSpacingOffest: CGFloat = 10.0
    static let buttonAlpha: CGFloat = 0.15
    static let indicatorCornerRadius: CGFloat = 25.0
    static let scrollViewHeightForViews: CGFloat = 715.0
    static let safeAreaOffsetMultiplier: CGFloat = 4.0
    static let closeButtonWeight: CGFloat = 42.0
    static let buttonWidth: CGFloat = 75.0
    static let buttonCorner: CGFloat = 20.0
    static let frameCornerRadius: CGFloat = 16.0
}

struct StringConstants {
    static let close = "🅧"
    static let account = "Account"
    static let support = "Support"
    static let submit = "SUBMIT"
    static let info = "Change password"
    static let contact = "Contact us"
    static let policy = "Privacy policy"
    static let terms = "Terms and conditions"
    static let about = "About"
    static let lekki = "lekki"
    static let status = "status"
    static let error = "Error"
    static let requiredFields = "All fields are required"
    static let invalidEmail = "Invalid email"
    static let shortPassword = "Password is too short. Min 7 characters"
    static let register = "Register"
    static let password = "Password"
    static let email = "Email"
    static let defaultFont = "Optima"
    static let done = "Done"
    static let forgotPassword = "Forgot password"
    static let success = "Success"
    static let newPassword = "New password"
    static let appPageUrl = "https://apps.apple.com/us/app/"
    static let appID = "id1521872386"
    static let share = "Get access to discounts on your favorite restaurants in Lagos. Check out Gidieats partner app!"
    static let copyright = "© 2020 Gidieats"
    static let cancel = "Cancel"
    static let infoEmail = "info@gidieats.com"
    static let details = "Details"
    static let payment = "Payment"
    static let okay = "Okay"
    static let businessName = "Business name"
    static let bankName = "Bank name"
    static let accountNumber = "Account Number"
    static let updateRestaurant = "Update my restaurant"
    static let progressScreen = "progressScreen"
    static let paymentInProgress = "paymentInProgress"
    static let restaurantInProgress = "restaurantInProgress"
    static let firstName = "First name"
    static let lastName = "Last name"
    static let code = "Code"
    static let businessEmail = "Business email"
    static let businessPhone = "Business phone"
    static let restaurantName = "Restaurant name"
    static let location = "Location"
}

struct APIConstants {
    static let baseURL = "https://api.gidieats.com/api/v1/"
    static let paystackSubaccountUrl = "https://api.paystack.co/subaccount"
    static let account = "accounts/"
    static let deals = "deals?"
    static let forgotPassword = "password/forgot"
    static let changedPassword = "Password changed"
    static let userId = "userId"
    static let emailAddress = "emailAddress"
    static let email = "email"
    static let password = "password"
    static let id = "id"
    static let firstName = "firstName"
    static let firstname = "firstname"
    static let token = "token"
    static let refreshToken = "refreshToken"
    static let bearer = "Bearer "
    static let serverError = "Server error"
    static let somethingWrong = "Something went wrong, please try again"
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
    static let noConnection = "Please check your internet connection"
    static let connectionError = "Connection error"
    static let authorization = "Authorization"
    static let checkConnection = "Server or connection error. Please refresh"
    static let dealSlug = "dealSlug"
    static let resendCode = "Resend code"
    static let isSuccess = "isSuccess"
    static let data = "data"
    static let errorMessages = "errorMessages"
    static let emptyValue = ""
    static let citySlug = "citySlug"
    static let itemsPerPage = "itemsPerPage"
    static let paystackSecretKey = "sk_live_77145d502367710b5b13fd4ae5b66ba7298f0aaf"
    static let paystackBankListUrl = "https://api.paystack.co/bank"
    static let codeIsResent = "Your code has been resent"
    static let roles = "roles"
    static let partner = "Partner"
    static let subaccountCode = "subaccountCode"
    static let restaurantSlug = "restaurantSlug"
    static let unauthorizedResponse = "401"
    static let badResponse = "400"
    static let forbidden = "403"
    static let get = "GET"
    static let savedProfilePicture = "savedProfilePicture"
    static let users = "users/"
    static let dealImageUrl = "dealImageUrl"
    static let purchases = "purchases"
    static let purchaseCode = "purchaseCode"
    static let unauthorized = "Authorization failed"
    static let reauthorize = "Please log in again"
}

struct ColorConstants {
    static let gidiGreen = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1).withAlphaComponent(0.85)
    static let gidiGray = UIColor.gray.withAlphaComponent(0.85)
}

/* Color scheme:
 
 Green: Cursors, tab tints and other buttons items - 35%
 White: Backgrounds and other views - 25%
 Black: Nav bar tints and text colors - 25%
 Gray: Placeholders and minor views - 14%
 Red: Very rare - 1%
 
 */

/*
 
 Fonts:
 Optima: All round and primary font - 95%
 Times: Few Labels and texts - 4%
 Other: 1%
 
 */
