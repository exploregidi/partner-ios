//
//  DealDetailsViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/30/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class DealDetailsViewController: UIViewController, UIPickerViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getDealDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.view.bounds
        view.contentInsetAdjustmentBehavior = .never
        view.backgroundColor = .white
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.contentSize = CGSize(width: UIScreen.main.bounds.width, height: Constants.dynamicHeight)
        return view
    }()
    
    let imageContainer = UIView()
    
    let productImages: CustomImageView = {
        let image = CustomImageView()
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        image.layer.cornerRadius = Constants.indicatorValue
        image.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
        return image
    }()
    
    let nameLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.biggerLabelFont)
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let locationLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        return label
    }()
    
    let originalAmount: AuthLabel = {
        let label = AuthLabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: Constants.bodyLabelFont)
        return label
    }()
    
    let discountedAmount: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont.systemFont(ofSize: Constants.biggerLabelFont, weight: .bold)
        label.textColor = ColorConstants.gidiGreen
        return label
    }()
    
    let expiryDateLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont.systemFont(ofSize: Constants.bodyLabelFont)
        label.textColor = .red
        return label
    }()
    
    let deleteDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Delete deal", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(deleteDeal), for: .touchUpInside)
        return button
    }()
    
    let detailsLabel: AuthLabel = {
        let label = AuthLabel()
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        label.textAlignment = .left
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let blocker = UIView()
    
    var name: String?
    var discount: String?
    var original: String?
    var expiryDate: String?
    var desc: String?
    var imageUrl: String?
    
    let datePicker = UIDatePicker()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    let transparentView = UIView()
    
    private func configureUI() {
        view.backgroundColor = .white
    }
    
    @objc func showDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.sizeToFit()
        
        toolbar.sizeToFit()
        
        blocker.translateAll()
        blocker.backgroundColor = .white
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneDatePicker))
        let expiryLabel = UIBarButtonItem(title: "Select expiry date", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelDatePicker))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        expiryLabel.isEnabled = false
        
        toolbar.setItems([cancelButton, flexibleSpace, expiryLabel, flexibleSpace, doneButton], animated: false)
        
        doneButton.tintColor = ColorConstants.gidiGreen
        expiryLabel.tintColor = ColorConstants.gidiGray
        cancelButton.tintColor = .black
        
        datePicker.isHidden = false
        toolbar.isHidden = false
        blocker.isHidden = false
        transparentView.isHidden = false
        
        transparentView.translateAll()
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(Constants.labelBackgroundAlpha)
        
        view.addSubview(transparentView)
        
        _ = transparentView.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        
        transparentView.addSubview(blocker)
        blocker.addSubview(datePicker)
        blocker.addSubview(toolbar)
        
        _ = blocker.anchor(left: transparentView.safeAreaLayoutGuide.leadingAnchor, bottom: transparentView.bottomAnchor, right: transparentView.safeAreaLayoutGuide.trailingAnchor, heightConstant: datePicker.frame.size.height + toolbar.frame.size.height)
        _ = datePicker.anchor(left: blocker.leadingAnchor, bottom: blocker.bottomAnchor, right: blocker.trailingAnchor)
        _ = toolbar.anchor(left: datePicker.leadingAnchor, bottom: datePicker.topAnchor, right: datePicker.trailingAnchor)
    }
    
    @objc func doneDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        expiryDate = formatter.string(from: datePicker.date)
        datePicker.isHidden = true
        toolbar.isHidden = true
        blocker.isHidden = true
        renewDeal()
    }
    
    @objc func cancelDatePicker() {
        datePicker.isHidden = true
        toolbar.isHidden = true
        blocker.isHidden = true
        transparentView.isHidden = true
    }
    
    func getDealDetails() {
        if Connectivity.isConnected() {
            self.networkLoaderStart()
            self.activityIndicatorStart()
            let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
            let urlString = APIConstants.baseURL + "deals/\(dealSlug)"
            guard let url = URL(string: urlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    _ = error
                    return
                }
                do {
                    let jsonData = try JSONDecoder().decode(DealDetailData.self, from: data!)
                    DispatchQueue.main.async {
                        if let image = jsonData.data.imageUrl {
                            self.productImages.load(urlString: image)
                        }
                        
                        self.nameLabel.text = jsonData.data.name
                        self.locationLabel.text = "\(jsonData.data.restaurant.city.name ?? APIConstants.emptyValue) | \(jsonData.data.restaurant.phoneNumber ?? APIConstants.emptyValue)"
                        
                        let originalPrice: NSMutableAttributedString = NSMutableAttributedString(string: "₦\(jsonData.data.valuePrice ?? APIConstants.emptyValue)")
                        originalPrice.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, originalPrice.length))
                        self.originalAmount.attributedText = originalPrice
                        
                        self.discountedAmount.text = "₦\(jsonData.data.discountPrice ?? APIConstants.emptyValue)"
                        
                        let expiry = jsonData.data.isExpired
                        
                        if expiry == true {
                            self.expiryDateLabel.text = "Expired"
                            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Renew deal", style: .plain, target: self, action: #selector(self.showDatePicker))
                        } else {
                            self.expiryDateLabel.text = "\(jsonData.data.expiryDate ?? APIConstants.emptyValue)"
                        }
                        self.detailsLabel.text = "\(jsonData.data.description ?? APIConstants.emptyValue)"
                        
                        self.name = jsonData.data.name
                        self.discount = jsonData.data.discountPrice?.replacingOccurrences(of: ",", with: APIConstants.emptyValue)
                        self.original = jsonData.data.valuePrice?.replacingOccurrences(of: ",", with: APIConstants.emptyValue)
                        self.desc = jsonData.data.description
                        self.imageUrl = jsonData.data.imageUrl
                        
                        let purchaseCode = UserDefaults.standard.object(forKey: APIConstants.purchaseCode) as? String ?? APIConstants.emptyValue
                        if purchaseCode != APIConstants.emptyValue {
                            self.deleteDealButton.isEnabled = false
                            self.deleteDealButton.backgroundColor = ColorConstants.gidiGreen.withAlphaComponent(Constants.buttonAlpha)
                        }
                        
                        self.imageContainer.viewContainer()
                        
                        self.view.addSubview(self.scrollView)
                        self.scrollView.addSubview(self.imageContainer)
                        self.imageContainer.addSubview(self.productImages)
                        self.scrollView.addSubview(self.nameLabel)
                        self.scrollView.addSubview(self.locationLabel)
                        self.scrollView.addSubview(self.originalAmount)
                        self.scrollView.addSubview(self.deleteDealButton)
                        self.scrollView.addSubview(self.detailsLabel)
                        
                        _ = self.scrollView.anchor(self.view.topAnchor, left: self.view.leadingAnchor, bottom: self.view.bottomAnchor, right: self.view.trailingAnchor)
                        _ = self.imageContainer.anchor(self.scrollView.topAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor)
                        
                        if UIDevice.current.userInterfaceIdiom == .pad {
                            _ = self.imageContainer.anchor(heightConstant: Constants.pictureHeightForPad)
                        } else {
                            _ = self.imageContainer.anchor(heightConstant: Constants.pictureHeight)
                        }
                        
                        _ = self.productImages.anchor(self.imageContainer.topAnchor, left: self.imageContainer.leadingAnchor, bottom: self.imageContainer.bottomAnchor, right: self.imageContainer.trailingAnchor)
                        _ = self.nameLabel.anchor(self.imageContainer.bottomAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, leftConstant: Constants.sideConstraintForLabel / Constants.viewContentMinDivider, rightConstant: Constants.sideConstraintForLabel / Constants.viewContentMinDivider)
                        _ = self.locationLabel.anchor(self.nameLabel.bottomAnchor, left: self.nameLabel.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.originalAmount.anchor(self.locationLabel.bottomAnchor, left: self.nameLabel.leadingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        
                        let labelStack = UIStackView(arrangedSubviews: [self.discountedAmount, self.expiryDateLabel])
                        labelStack.distribution = .equalSpacing
                        
                        self.scrollView.addSubview(labelStack)
                        
                        _ = labelStack.anchor(self.originalAmount.bottomAnchor, left: self.nameLabel.leadingAnchor, right: self.nameLabel.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                        _ = self.deleteDealButton.anchor(labelStack.bottomAnchor, left: labelStack.leadingAnchor, right: labelStack.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels, heightConstant: Constants.smallButtonSize)
                        _ = self.detailsLabel.anchor(self.deleteDealButton.bottomAnchor, left: self.deleteDealButton.leadingAnchor, bottom: self.scrollView.bottomAnchor, right: labelStack.trailingAnchor, topConstant: Constants.topConstraintBetweenLabels)
                    }
                    self.networkLoaderStop()
                    self.activityIndicatorStop()
                } catch _ {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                        self.navigationController?.navigationBar.tintColor = .black
                    }
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                self.internetCheck(text: APIConstants.checkConnection)
                self.networkLoaderStop()
                self.activityIndicatorStop()
                self.navigationController?.navigationBar.tintColor = .black
            }
        }
    }
    
    @objc func deleteDeal() {
        let alertController = UIAlertController(title: "Confirm delete", message: "Delete deal?", preferredStyle: .alert)
        let continueButton = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            
            let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
            let dealSlug = UserDefaults.standard.object(forKey: APIConstants.dealSlug) as? String ?? APIConstants.emptyValue
            
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            AF.request(APIConstants.baseURL + "deals/\(dealSlug)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                self.alertAndPop(message: "Deleted", title: StringConstants.success)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.deleteDeal()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
        
        let cancelButton = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertController.addAction(continueButton)
        alertController.addAction(cancelButton)
        presentViewController(viewController: alertController)
    }
    
    @objc func renewDeal() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        self.networkLoaderStart()
        self.view.isUserInteractionEnabled = false
        self.activityIndicatorStart()
        
        let parameters: [String: Any] = [
            "name": name ?? APIConstants.emptyValue,
            "description": desc ?? APIConstants.emptyValue,
            "discountPrice": (discount! as NSString).doubleValue,
            "valuePrice": (original! as NSString).doubleValue,
            "expiryDate": expiryDate ?? APIConstants.emptyValue,
            "imageUrl": imageUrl ?? APIConstants.emptyValue
        ]
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                    APIConstants.authorization: APIConstants.bearer + token
        ]
        
        AF.request(APIConstants.baseURL + "deals", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if Connectivity.isConnected() { //Check connectivity status
                let info = serializeResponse(data: response.data ?? Data())
                switch response.result {
                case .success(_):
                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
                        if info[APIConstants.data] != nil {
                            self.alertAndPop(message: "Deal has been renewed", title: StringConstants.success)
                        }
                    } else {
                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                        let message = userData.joined()
                        self.alert(message: message, title: StringConstants.error)
                    }
                case .failure (_):
                    let response = response.debugDescription
                    if response.contains(APIConstants.unauthorizedResponse) {
                        newToken()
                        
                        let time = DispatchTime.now() + 3
                        DispatchQueue.main.asyncAfter(deadline: time) {
                            self.renewDeal()
                        }
                    } else if response.contains(APIConstants.badResponse) {
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                        UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                        
                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                    } else {
                        DispatchQueue.main.async {
                            self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                        }
                    }
                }
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
            self.activityIndicatorStop()
            self.view.isUserInteractionEnabled = true
            self.networkLoaderStop()
        }
    }
}
