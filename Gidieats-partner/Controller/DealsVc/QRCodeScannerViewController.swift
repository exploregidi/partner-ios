//
//  QRCodeScannerViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 6/12/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class QRCodeScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    let squareImage: UIImageView = {
        let image = UIImageView()
        image.layer.borderColor = ColorConstants.gidiGreen.cgColor
        image.layer.borderWidth = 2
        image.backgroundColor = .clear
        return image
    }()
    
    private func configureUI() {
        view.backgroundColor = .black
        
        navigationController?.navigationBar.tintColor = .white
        
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.addSubview(squareImage)
        view.bringSubviewToFront(squareImage)
        
        _ = squareImage.anchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, widthConstant: 300, heightConstant: 300)
        
        captureSession.startRunning()
    }
    
    func failed() {
        alert(message: "Your device does not support scanning a code from an item. Please use a device with a camera.", title: "Scanning not supported")
        captureSession = nil
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        self.activityIndicatorStart()
        self.view.isUserInteractionEnabled = false
        self.networkLoaderStart()
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                    APIConstants.authorization: APIConstants.bearer + token
        ]
        
        AF.request(APIConstants.baseURL + "purchases/\(code)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if Connectivity.isConnected() { //Check connectivity status
                let info = serializeResponse(data: response.data ?? Data())
                switch response.result {
                case .success(_):
                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
                        if info[APIConstants.data] != nil {
                            let userData = info[APIConstants.data] as? [String: Any]
                            let dealData = userData?["deal"] as? [String: Any]
                            let redeemStatus = userData?["isRedeemed"] as? Bool
                            let dealName = dealData?["name"] as? String
                            var message: String?
                            
                            if redeemStatus == true {
                                message = "Deal (\(dealName ?? APIConstants.emptyValue)) has already been used."
                                DispatchQueue.main.async {
                                    let alertController = UIAlertController(title: "Gidi Eats", message: message, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
                                    })
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            } else {
                                message = "Deal (\(dealName ?? APIConstants.emptyValue)) has not been used yet."
                                DispatchQueue.main.async {
                                    let alertController = UIAlertController(title: "Gidi Eats", message: "Deal: \(dealName ?? APIConstants.emptyValue)\n\n\(message ?? APIConstants.emptyValue)", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
                                    })
                                    let redeemAction = UIAlertAction(title: "Redeem", style: .default, handler: { action in
                                        
                                        let parameters: [String: Any] = [
                                            "isRedeemed": true
                                        ]
                                        
                                        self.activityIndicatorStart()
                                        
                                        AF.request(APIConstants.baseURL + "purchases/\(code)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { otherResponse in
                                            
                                            let otherInfo = serializeResponse(data: response.data ?? Data())
                                            switch otherResponse.result {
                                            case .success(_):
                                                if otherInfo[APIConstants.isSuccess] as? Bool ?? false == true {
                                                    if otherInfo[APIConstants.data] != nil {
                                                        self.alertAndPop(message: "Deal \(dealName ?? APIConstants.emptyValue) redeemed sucessfully", title: "Gidi Eats")
                                                    }
                                                } else {
                                                    let userData = otherInfo[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                                                    let message = userData.joined()
                                                    self.alert(message: message, title: StringConstants.error)
                                                }
                                            case .failure (_):
                                                let response = response.debugDescription
                                                if response.contains(APIConstants.unauthorizedResponse) {
                                                    newToken()
                                                    
                                                    let time = DispatchTime.now() + 3
                                                    DispatchQueue.main.asyncAfter(deadline: time) {
                                                        self.found(code: code)
                                                    }
                                                } else if response.contains(APIConstants.badResponse) {
                                                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                                                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                                                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                                                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                                                    
                                                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                                                } else {
                                                    DispatchQueue.main.async {
                                                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                                                    }
                                                }
                                            }
                                            self.activityIndicatorStop()
                                        }
                                    })
                                    alertController.addAction(OKAction)
                                    alertController.addAction(redeemAction)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                    } else {
                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                        let message = userData.joined()
                        self.alert(message: message, title: StringConstants.error)
                    }
                case .failure (_):
                    let response = response.debugDescription
                    if response.contains(APIConstants.unauthorizedResponse) {
                        newToken()
                        
                        let time = DispatchTime.now() + 3
                        DispatchQueue.main.asyncAfter(deadline: time) {
                            self.found(code: code)
                        }
                    } else if response.contains(APIConstants.badResponse) {
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                        UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                        
                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                    } else {
                        DispatchQueue.main.async {
                            self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                        }
                    }
                }
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
            self.activityIndicatorStop()
            self.view.isUserInteractionEnabled = true
            self.networkLoaderStop()
        }
    }
}
