//
//  AddDealsViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 6/11/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire
import Cloudinary

class AddDealsViewController: UIViewController, UIPickerViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.dealImageUrl)
        selectPicture.backgroundColor = ColorConstants.gidiGray
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.scrollView.endEditing(true)
    }
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.view.bounds
        view.contentInsetAdjustmentBehavior = .never
        view.backgroundColor = .white
        view.contentSize = CGSize(width: UIScreen.main.bounds.width, height: Constants.scrollViewHeightForViews)
        return view
    }()
    
    let statusBar =  UIView()
    
    let datePicker = UIDatePicker()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    var imagePicker = UIImagePickerController()
    
    let nameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: "Deal name ex. 'Dinner for 2'", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .sentences
        field.autocorrectionType = .yes
        field.returnKeyType = .next
        return field
    }()
    
    let descriptionField: UITextView = {
        let field = UITextView()
        field.attributedText = NSAttributedString(string: "Enter a detailed description of the deal. What's included?", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .sentences
        field.layer.borderWidth = Constants.dynamicHeight
        field.backgroundColor = .clear
        field.tintColor = ColorConstants.gidiGreen
        field.textAlignment = .left
        field.autocorrectionType = .yes
        field.font = UIFont.systemFont(ofSize: Constants.fontForTextField, weight: .regular)
        field.font = UIFont(name: StringConstants.defaultFont, size: Constants.fontForTextField)
        return field
    }()
    
    let discountPriceField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: "New price", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .decimalPad
        return field
    }()
    
    let valuePriceField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: "Originial price", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .decimalPad
        return field
    }()
    
    let expiryField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: "Deal expires when?", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.returnKeyType = .next
        return field
    }()
    
    let selectPicture: AuthButton = {
        let button = AuthButton()
        button.setTitle("Upload image", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGray
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(selectDealPicture), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    func showDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.sizeToFit()
        
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        expiryField.inputAccessoryView = toolbar
        expiryField.inputView = datePicker
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
    }
    
    @objc func doneDatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        expiryField.text = formatter.string(from: datePicker.date)
        
        expiryField.resignFirstResponder()
    }
    
    @objc func cancelDatePicker() {
        expiryField.attributedPlaceholder = NSAttributedString(string: "Deal expires when?", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        expiryField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == expiryField {
            showDatePicker()
        }
        return true
    }
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descriptionField {
            if textView.text == "Enter a detailed description of the deal. What's included?" {
                textView.text = ""
                textView.textColor = .black
            }
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if textView == descriptionField {
            if textView.text == "" {
                textView.text = "Enter a detailed description of the deal. What's included?"
                textView.textColor = ColorConstants.gidiGray
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameField {
            discountPriceField.becomeFirstResponder()
        }
        return true
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = .clear
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        
        navigationItem.title = "Add a new deal"
        
        imagePicker.delegate = self
        
        expiryField.delegate = self
        descriptionField.delegate = self
        
        nameField.delegate = self
        
        view.addSubview(scrollView)
        scrollView.addSubview(nameField)
        scrollView.addSubview(descriptionField)
        scrollView.addSubview(discountPriceField)
        scrollView.addSubview(valuePriceField)
        scrollView.addSubview(expiryField)
        scrollView.addSubview(selectPicture)
        scrollView.addSubview(submitButton)
        
        _ = nameField.anchor(scrollView.topAnchor, left: scrollView.safeAreaLayoutGuide.leadingAnchor, right: scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: ((navigationController?.navigationBar.frame.size.height)! + (statusBar.frame.size.height + Constants.buttonCornerRadius)), leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = discountPriceField.anchor(nameField.bottomAnchor, left: nameField.leadingAnchor, topConstant: Constants.spacingBetweenTextFields, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing),  heightConstant: Constants.textFieldHeight)
        _ = valuePriceField.anchor(nameField.bottomAnchor, right: nameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = expiryField.anchor(valuePriceField.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = descriptionField.anchor(expiryField.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight * 2.5)
        _ = selectPicture.anchor(descriptionField.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = submitButton.anchor(selectPicture.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
    }
    
    @objc func selectDealPicture() {
        self.imagePicker.allowsEditing = true
        self.imagePicker.modalPresentationStyle = .overFullScreen
        self.imagePicker.sourceType = .photoLibrary
        self.presentViewController(viewController: self.imagePicker)
    }
    
    @objc func submitForm() {
        self.view.endEditing(true)
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        
        let name = nameField.text
        let descriptions = descriptionField.text
        let discount = discountPriceField.text
        let value = valuePriceField.text
        let expiry = expiryField.text
        let imageUrl = UserDefaults.standard.object(forKey: APIConstants.dealImageUrl) as? String ?? APIConstants.emptyValue
        
        let discountFigure = (discount! as NSString).doubleValue
        let valueFigure = (value! as NSString).doubleValue
        
        if (name!.isEmpty ) {
            nameField.attributedPlaceholder = NSAttributedString(string: "Deal name ex. 'Dinner for 2'", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (descriptions!.isEmpty) {
            self.alert(message: "Enter a description for the deal.", title: StringConstants.error)
        } else if (value!.isEmpty) {
            valuePriceField.attributedPlaceholder = NSAttributedString(string: "Original price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (discount!.isEmpty) {
            discountPriceField.attributedPlaceholder = NSAttributedString(string: "New price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (expiry!.isEmpty) {
            expiryField.attributedPlaceholder = NSAttributedString(string: "Deal expires when?", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (descriptions?.count ?? 0 < 80) {
            self.alert(message: "Description is too short. At least 80 characters", title: StringConstants.error)
        } else if (discountFigure > valueFigure) {
            self.alert(message: "New price can't be higher than the original price", title: StringConstants.error)
        } else if (discountFigure > (valueFigure * 0.85)) {
            self.alert(message: "Discount price can't be more than ₦\(valueFigure * 0.85) (15% off)", title: StringConstants.error)
        } else if (imageUrl == APIConstants.emptyValue) {
            self.alert(message: "Upload an image", title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                "name": name!,
                "description": descriptions!,
                "discountPrice": discountFigure,
                "valuePrice": valueFigure,
                "expiryDate": expiry!,
                "imageUrl": imageUrl
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            AF.request(APIConstants.baseURL + "deals", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                let userData = info[APIConstants.data] as? String
                                
                                self.alertAndPop(message: userData ?? "Deal has been added", title: StringConstants.success)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure (_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.submitForm()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        self.activityIndicatorStart()
        self.view.isUserInteractionEnabled = false
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            let imgData = image.pngData()
            let config = CLDConfiguration(cloudName: "explore-gidi", apiKey: "163752141535158")
            let cloudinary = CLDCloudinary(configuration: config)
            cloudinary.createUploader().upload(data: imgData!, uploadPreset: "image-preset") { (response, error) in
                if let error = error {
                    _ = error
                    self.alert(message: "Please upload a smaller sized photo", title: StringConstants.error)
                } else {
                    if let response = response {
                        let imageUrl = response.secureUrl
                        UserDefaults.standard.set(imageUrl, forKey: APIConstants.dealImageUrl)
                        self.view.isUserInteractionEnabled = true
                        self.activityIndicatorStop()
                        self.selectPicture.backgroundColor = ColorConstants.gidiGreen
                    }
                }
            }
            dismiss(animated: true, completion: nil)
        }
    }
}
