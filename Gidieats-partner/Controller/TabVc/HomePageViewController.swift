//
//  HomePageViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class HomePageViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSubaccountCode()
        tabBarController?.tabBar.isHidden = false
    }
    
    var deal: [Deal]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    let statusBar =  UIView()
    
    let homeRowId = "homeRowId"
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.text = "No deals"
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deal?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeRowId, for: indexPath) as! GenericCollectionViewCell
        cell.deal = deal?[indexPath.item]
        
        if let expiryStatus = deal?[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            cell.expiryLabel.layer.backgroundColor = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.layer.backgroundColor = UIColor.black.cgColor
                        cell.expiryLabel.layer.cornerRadius = Constants.tableHeightSpacingOffest
                        cell.expiryLabel.text = "Expired"
                        cell.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.imageView.topAnchor, right: cell.imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonFormWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(deal?[indexPath.item].slug, forKey: APIConstants.dealSlug)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    @objc func fetchDeals() {
        self.activityIndicatorStart()
        self.networkLoaderStart()
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data!)
                UserDefaults.standard.set(jsonData.data.restaurant.slug, forKey: APIConstants.restaurantSlug)
                let restaurantSlug = jsonData.data.restaurant.slug ?? APIConstants.emptyValue
                
                if Connectivity.isConnected() {
                    let urlString = APIConstants.baseURL + APIConstants.deals + "\(APIConstants.itemsPerPage)\(Constants.defaultItems)&restaurantSlug=\(restaurantSlug)"
                    guard let url = URL(string: urlString) else { return }
                    URLSession.shared.dataTask(with: url) { (data, response, error) in
                        if error != nil {
                            _ = error
                            return
                        }
                        do {
                            let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                            for dictionary in jsonData.data {
                                let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                                self.deal?.append(section)
                            }
                            self.networkLoaderStop()
                            self.activityIndicatorStop()
                            
                            DispatchQueue.main.async {
                                if self.deal?.count ?? 0 == 0 {
                                    self.addEmptyDealPrompt()
                                } else {
                                    self.removeEmptyDealPrompt()
                                }
                            }
                        } catch _ {
                            DispatchQueue.main.async {
                                self.internetCheck(text: APIConstants.checkConnection)
                                self.networkLoaderStop()
                                self.activityIndicatorStop()
                            }
                        }
                    }.resume()
                } else {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                    }
                }
                self.networkLoaderStop()
                self.activityIndicatorStop()
            } catch _ {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.fetchDeals()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                    }
                }
            }
        }.resume()
    }
    
    func getSubaccountCode() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let restaurantSlug = UserDefaults.standard.object(forKey: APIConstants.restaurantSlug) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + "restaurants/\(restaurantSlug)"
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(RestaurantDetailData.self, from: data!)
                UserDefaults.standard.set(jsonData.data.subAccountCode, forKey: APIConstants.subaccountCode)
                self.networkLoaderStop()
            } catch _ {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getSubaccountCode()
                    }
                }
            }
        }.resume()
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        collectionView.backgroundColor = .white
        
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = titleColor
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = .clear
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.barTintColor = .white
        
        navigationController?.navigationBar.shadowImage = UIImage()
        tabBarController?.tabBar.shadowImage = UIImage()
        tabBarController?.tabBar.backgroundImage = UIImage()
        tabBarController?.tabBar.backgroundColor = .white
        
        navigationItem.title = "Deals"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Redeem", style: .plain, target: self, action: #selector(redeemDeal))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadApp)), UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addDeal))]
        
        collectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: homeRowId)
    }
    
    @objc func reloadApp() {
        switchView(value: true)
    }
    
    @objc func redeemDeal() {
        pushViewController(viewController: QRCodeScannerViewController())
    }
    
    @objc func addDeal() {
        pushViewController(viewController: AddDealsViewController())
    }
}
