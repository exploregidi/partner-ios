//
//  WelcomePageViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/11/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit

class WelcomePageViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "shake")
        return image
    }()
    
    let exploreLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = .black
        label.textAlignment = .left
        label.text = "GIDI"
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.labelFont)
        return label
    }()
    
    let gidiLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .left
        label.text = "EATS"
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.labelFont)
        return label
    }()
    
    let images: [UIImage] = [UIImage(named: "scones")!, UIImage(named: "pancake")!, UIImage(named: "cake")!, UIImage(named: "shake")!]
    
    let getStartedLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = .black
        label.textAlignment = .left
        label.text = "Get started with Gidi Eats partner"
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        return label
    }()
    
    let start: AuthButton = {
        let button = AuthButton()
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.buttonCorner
        button.setTitle("CONTINUE", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.addTarget(self, action: #selector(login), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        view.backgroundColor = .white
        
        backgroundImage.animationImages = images
        backgroundImage.animationDuration = Constants.animationTiming
        backgroundImage.animationRepeatCount = Constants.labelNumberOfLines
        
        let time = DispatchTime.now() + Constants.defaultTime
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.backgroundImage.startAnimating()
        }
        
        view.addSubview(backgroundImage)
        backgroundImage.addSubview(exploreLabel)
        backgroundImage.addSubview(gidiLabel)
        view.addSubview(getStartedLabel)
        view.addSubview(start)
        
        _ = exploreLabel.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, topConstant: Constants.regularSideConstraint, leftConstant: Constants.regularSideConstraint)
        _ = gidiLabel.anchor(exploreLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, leftConstant: Constants.regularSideConstraint)
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.trailingAnchor, bottomConstant: Constants.bottomFromViewConstraint)
        _ = getStartedLabel.anchor(backgroundImage.bottomAnchor, centerX: view.centerXAnchor, topConstant: Constants.regularSideConstraint)
        _ = start.anchor(getStartedLabel.bottomAnchor, left: exploreLabel.leadingAnchor, centerX: view.centerXAnchor, topConstant: Constants.buttonSideConstraint, heightConstant: Constants.textFieldHeight)
    }
    
    @objc func login() {
        presentViewController(viewController: LoginViewController())
    }
}
