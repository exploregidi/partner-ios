//
//  PaymentAccountViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class PaymentAccountViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        UserDefaults.standard.set(StringConstants.paymentInProgress, forKey: StringConstants.progressScreen)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBanks()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var banks: [Banks]? = []
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
        
    let paymentLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = StringConstants.payment
        return label
    }()
    
    let businessNameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.businessName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .sentences
        return field
    }()
    
    let bankNameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.bankName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        return field
    }()
    
    let dropDown: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "dropdown")
        return image
    }()
    
    let accountNumberField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.accountNumber, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .numberPad
        return field
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    let progressLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = "•2•"
        label.layer.backgroundColor = UIColor.black.cgColor
        label.layer.cornerRadius = Constants.topConstraintForButton
        label.textColor = .white
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        return label
    }()
    
    let percentCharge: Float = Constants.percentCharge
    
    let bankPicker = UIPickerView()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return banks?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = banks?[row].name
        return row
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        if (businessNameField.text!.isEmpty) {
            businessNameField.attributedPlaceholder = NSAttributedString(string: StringConstants.businessName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (bankNameField.text!.isEmpty) {
            bankNameField.attributedPlaceholder = NSAttributedString(string: StringConstants.bankName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (accountNumberField.text!.isEmpty) {
            accountNumberField.attributedPlaceholder = NSAttributedString(string: StringConstants.accountNumber, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
            let parameters: [String: Any] = ["business_name": businessNameField.text!,
                                             "primary_contact_email": email,
                                             "percentage_charge": percentCharge,
                                             "settlement_bank": bankNameField.text!,
                                             "account_number": accountNumberField.text!,
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + APIConstants.paystackSecretKey
            ]
            
            AF.request(APIConstants.paystackSubaccountUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        let data = info["data"] as? [String: Any]
                        let subaccountCode = data?["subaccount_code"]
                        UserDefaults.standard.set(subaccountCode, forKey: APIConstants.subaccountCode)
                        self.presentViewController(viewController: AddRestaurantViewController())
                    case .failure(_):
                        self.alert(message: "Please try again. Be sure to input a valid account", title: StringConstants.error)
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
    
    func getBanks() {
        let urlString = APIConstants.paystackBankListUrl
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(BanksData.self, from: data!)
                for dictionary in jsonData.data {
                    let section = Banks(name: dictionary.name, slug: dictionary.slug, code: dictionary.code, longcode: dictionary.longcode, gateway: dictionary.gateway, payWithBank: dictionary.payWithBank, active: dictionary.active, isDeleted: dictionary.isDeleted, country: dictionary.country, currency: dictionary.currency, type: dictionary.type, id: dictionary.id, createdAt: dictionary.createdAt, updatedAt: dictionary.updatedAt)
                    self.banks?.append(section)
                }
            } catch _ {}
        }.resume()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == bankNameField {
            showBankPicker()
        }
        return true
    }
    
    func showBankPicker() {
        bankPicker.dataSource = self
        bankPicker.delegate = self
        bankPicker.showsSelectionIndicator = true
        bankPicker.sizeToFit()
        
        bankNameField.inputView = bankPicker
        bankNameField.tintColor = .clear
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.translateAll()
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneCategory))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelCategory))
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        bankNameField.inputAccessoryView = toolbar
    }
    
    @objc func doneCategory() {
        if banks?.count == 0 {
            bankNameField.text = APIConstants.emptyValue
        } else {
            bankNameField.text = banks?[bankPicker.selectedRow(inComponent: 0)].name
        }
        bankNameField.resignFirstResponder()
    }
    
    @objc func cancelCategory() {
        bankNameField.resignFirstResponder()
        bankNameField.attributedPlaceholder = NSAttributedString(string: StringConstants.bankName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        modalTransitionStyle = .crossDissolve
        
        bankNameField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(paymentLabel)
        view.addSubview(businessNameField)
        view.addSubview(bankNameField)
        view.addSubview(accountNumberField)
        view.addSubview(submitButton)
        view.addSubview(progressLabel)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = paymentLabel.anchor(view.topAnchor, centerX: view.safeAreaLayoutGuide.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels * Constants.safeAreaOffsetMultiplier)
        _ = businessNameField.anchor(paymentLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = bankNameField.anchor(businessNameField.bottomAnchor, left: businessNameField.leadingAnchor, right: businessNameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = accountNumberField.anchor(bankNameField.bottomAnchor, left: bankNameField.leadingAnchor, right: bankNameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = submitButton.anchor(accountNumberField.bottomAnchor, left: accountNumberField.leadingAnchor, right: accountNumberField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = progressLabel.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: -Constants.indexPathDivider, widthConstant: Constants.smallButtonSize, heightConstant: Constants.indicatorCornerRadius)

        bankNameField.addSubview(dropDown)
        _ = dropDown.anchor(right: bankNameField.trailingAnchor, centerY: bankNameField.centerYAnchor, rightConstant: Constants.dropDownTrail)
    }
}
