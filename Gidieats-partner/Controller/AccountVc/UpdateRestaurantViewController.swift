//
//  UpdateRestaurantViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class UpdateRestaurantViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.clipsToBounds = true
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCities()
        getSubaccountCode()
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    } //Hide keyboard on screen tap
    
    var cities: [Cities]? = []
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let restaurantLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = StringConstants.updateRestaurant
        return label
    }()
    
    let nameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.restaurantName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .words
        return field
    }()
    
    let locationField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        return field
    }()
    
    let dropDown: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "dropdown")
        return image
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    let myRestaurantButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("MY RESTAURANT INFO", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(getRestaurantDetails), for: .touchUpInside)
        return button
    }()
    
    let myPaymentButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("MY PAYMENT INFO", for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(getPaymentDetails), for: .touchUpInside)
        return button
    }()
    
    let locationPicker = UIPickerView()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = cities?[row].name
        return row
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == locationField {
            showCategoryPicker()
        }
        return true
    }
    
    func showCategoryPicker() {
        locationPicker.dataSource = self
        locationPicker.delegate = self
        locationPicker.showsSelectionIndicator = true
        locationPicker.sizeToFit()
        
        locationField.inputView = locationPicker
        locationField.tintColor = .clear
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.translateAll()
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneCategory))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelCategory))
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        locationField.inputAccessoryView = toolbar
    }
    
    @objc func doneCategory() {
        if cities?.count == 0 {
            locationField.text = APIConstants.emptyValue
        } else {
            locationField.text = cities?[locationPicker.selectedRow(inComponent: 0)].name ?? APIConstants.emptyValue
            UserDefaults.standard.set(cities?[locationPicker.selectedRow(inComponent: 0)].slug, forKey: APIConstants.citySlug)
        }
        locationField.resignFirstResponder()
    }
    
    @objc func cancelCategory() {
        locationField.resignFirstResponder()
        locationField.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
    }
    
    private func configureUI() {        
        view.backgroundColor = .white
        
        locationField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(restaurantLabel)
        view.addSubview(nameField)
        view.addSubview(locationField)
        view.addSubview(submitButton)
        view.addSubview(myRestaurantButton)
        view.addSubview(myPaymentButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = restaurantLabel.anchor(view.safeAreaLayoutGuide.topAnchor, centerX: view.safeAreaLayoutGuide.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels)
        _ = nameField.anchor(restaurantLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = locationField.anchor(nameField.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = submitButton.anchor(locationField.bottomAnchor, left: locationField.leadingAnchor, right: locationField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = myRestaurantButton.anchor(submitButton.bottomAnchor, left: submitButton.leadingAnchor, right: submitButton.trailingAnchor, topConstant: Constants.topConstraintForButton, heightConstant: Constants.smallButtonSize)
        _ = myPaymentButton.anchor(myRestaurantButton.bottomAnchor, left: myRestaurantButton.leadingAnchor, right: myRestaurantButton.trailingAnchor, topConstant: Constants.minConstraintOffset, heightConstant: Constants.smallButtonSize)
        
        locationField.addSubview(dropDown)
        _ = dropDown.anchor(right: locationField.trailingAnchor, centerY: locationField.centerYAnchor, rightConstant: Constants.dropDownTrail)
    }
    
    func getCities() {
        let urlString = APIConstants.baseURL + "cities"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(CitiesData.self, from: data!)
                self.cities = [Cities]()
                for dictionary in jsonData.data {
                    let section = Cities(deals: dictionary.deals, slug: dictionary.slug, name: dictionary.name)
                    self.cities?.append(section)
                }
            } catch _ {}
        }.resume()
    }
    
    func getSubaccountCode() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let restaurantSlug = UserDefaults.standard.object(forKey: APIConstants.restaurantSlug) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + "restaurants/\(restaurantSlug)"
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(RestaurantDetailData.self, from: data!)
                UserDefaults.standard.set(jsonData.data.subAccountCode, forKey: APIConstants.subaccountCode)
                self.networkLoaderStop()
            } catch _ {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getSubaccountCode()
                    }
                }
            }
        }.resume()
    }
    
    @objc func getRestaurantDetails() {
        DispatchQueue.main.async {
            self.networkLoaderStart()
            self.activityIndicatorStart()
            self.view.isUserInteractionEnabled = false
        }
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data!)
                UserDefaults.standard.set(jsonData.data.restaurant.slug, forKey: APIConstants.restaurantSlug)
                let restaurantSlug = jsonData.data.restaurant.slug ?? APIConstants.emptyValue
                
                if Connectivity.isConnected() {
                    let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
                    let urlString = APIConstants.baseURL + "restaurants/\(restaurantSlug)"
                    guard let urlRequest = URL(string: urlString) else { return }
                    var request = URLRequest(url: urlRequest)
                    request.httpMethod = APIConstants.get
                    request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
                    request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
                    URLSession.shared.dataTask(with: request) { (data, response, error) in
                        if error != nil {
                            _ = error
                            return
                        }
                        do {
                            let jsonData = try JSONDecoder().decode(RestaurantDetailData.self, from: data!)
                            
                            DispatchQueue.main.async {
                                self.alert(message: "Business name: \(jsonData.data.name ?? APIConstants.emptyValue)\nLocation: \(jsonData.data.city.name ?? APIConstants.emptyValue)", title: APIConstants.emptyValue)
                            }
                            DispatchQueue.main.async {
                                self.view.isUserInteractionEnabled = true
                                self.activityIndicatorStop()
                                self.networkLoaderStop()
                            }
                        } catch _ {
                            let response = response.debugDescription
                            if response.contains(APIConstants.unauthorizedResponse) {
                                newToken()
                                
                                let time = DispatchTime.now() + 3
                                DispatchQueue.main.asyncAfter(deadline: time) {
                                    self.getRestaurantDetails()
                                }
                            } else if response.contains(APIConstants.badResponse) {
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                                UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                                
                                self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                            } else {
                                DispatchQueue.main.async {
                                    self.alert(message: "You haven't set a restaurant yet", title: "No info")
                                }
                            }
                            self.view.isUserInteractionEnabled = true
                            self.activityIndicatorStop()
                            self.networkLoaderStop()
                        }
                    }.resume()
                } else {
                    DispatchQueue.main.async {
                        self.alert(message: "Please check your connection", title: "Connection error")
                    }
                }
                self.getSubaccountCode()
                self.networkLoaderStop()
            } catch _ {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    newToken()
                    
                    let time = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.getRestaurantDetails()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                }
            }
        }.resume()
    }
    
    @objc func getPaymentDetails() {
        activityIndicatorStart()
        let subaccountCode = UserDefaults.standard.object(forKey: APIConstants.subaccountCode) as? String ?? APIConstants.emptyValue
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                    APIConstants.authorization: APIConstants.bearer + APIConstants.paystackSecretKey
        ]
        
        AF.request("https://api.paystack.co/subaccount/\(subaccountCode)", method: .get, parameters: nil, headers: headers).responseJSON { response in
            let info = serializeResponse(data: response.data ?? Data())
            switch response.result {
            case .success(_):
                let data = info["data"] as? [String: Any]
                let bankName = data?["settlement_bank"] as? String ?? APIConstants.emptyValue
                let accountNumber = data?["account_number"] as? String ?? APIConstants.emptyValue
                DispatchQueue.main.async {
                    self.alert(message: "Bank: \(bankName)\nAccount number: \(accountNumber)\nUnique bank code: \(subaccountCode)\n\nIf you need to change your payment bank, please contact us through our support page. Thanks", title: APIConstants.emptyValue)
                }
                self.activityIndicatorStop()
            case .failure(_):
                self.alert(message: "Something went wrong. Please try later", title: "Error")
                self.activityIndicatorStop()
            }
        }
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        let name = nameField.text
        let location = UserDefaults.standard.object(forKey: APIConstants.citySlug) as? String ?? APIConstants.emptyValue
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        
        if (name!.isEmpty) {
            nameField.attributedPlaceholder = NSAttributedString(string: StringConstants.restaurantName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (location.isEmpty) {
            locationField.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                "name": name!,
                "imageUrl": "https://gidieats.com",
                "citySlug": location
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            //Alamofire post request
            AF.request(APIConstants.baseURL + "users/" + userId + "/restaurant", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data()) //Get raw data from backend
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil { //as? NSNull != NSNull.init()
                                let userData = info[APIConstants.data] as! [String: Any]
                                let restaurantSlug = userData["slug"] as? String ?? APIConstants.emptyValue
                                UserDefaults.standard.set(restaurantSlug, forKey: APIConstants.restaurantSlug)
                                let subaccountCode = UserDefaults.standard.object(forKey: APIConstants.subaccountCode) ?? APIConstants.emptyValue
                                
                                let otherParameters: [String: Any] = [
                                    "subAccountCode": subaccountCode
                                ]
                                
                                let otherHeaders: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                                 APIConstants.authorization: APIConstants.bearer + token
                                ]
                                
                                AF.request(APIConstants.baseURL + "restaurants/\(restaurantSlug)/sub-account-code", method: .put, parameters: otherParameters, encoding: JSONEncoding.default, headers: otherHeaders).responseJSON { otherResponse in
                                    let otherInfo = serializeResponse(data: otherResponse.data ?? Data())
                                    switch otherResponse.result {
                                    case .success(_):
                                        if otherInfo[APIConstants.isSuccess] as? Bool ?? false == true {
                                            self.alertAndPop(message: "Restaurant updated", title: StringConstants.success)
                                        } else {
                                            let userData = otherInfo[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                                            let message = userData.joined()
                                            self.alert(message: message, title: StringConstants.error)
                                        }
                                    case .failure(_):
                                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                                    }
                                }
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.submitForm()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
}
