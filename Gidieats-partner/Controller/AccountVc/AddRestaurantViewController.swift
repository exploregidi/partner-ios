//
//  AddRestaurantViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class AddRestaurantViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        UserDefaults.standard.set(StringConstants.restaurantInProgress, forKey: StringConstants.progressScreen)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCities()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    } //Hide keyboard on screen tap
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var cities: [Cities]? = []
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let restaurantLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = "Add Restaurant"
        return label
    }()
    
    let nameField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.restaurantName, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.autocapitalizationType = .sentences
        return field
    }()
    
    let locationField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        return field
    }()
    
    let dropDown: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "dropdown")
        return image
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    let progressLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = "••3"
        label.layer.backgroundColor = UIColor.black.cgColor
        label.layer.cornerRadius = Constants.topConstraintForButton
        label.textColor = .white
        label.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        return label
    }()
    
    let locationPicker = UIPickerView()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = cities?[row].name
        return row
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == locationField {
            showCategoryPicker()
        }
        return true
    }
    
    func showCategoryPicker() {
        locationPicker.dataSource = self
        locationPicker.delegate = self
        locationPicker.showsSelectionIndicator = true
        locationPicker.sizeToFit()
        
        locationField.inputView = locationPicker
        locationField.tintColor = .clear
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.translateAll()
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneCategory))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelCategory))
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        locationField.inputAccessoryView = toolbar
    }
    
    @objc func doneCategory() {
        if cities?.count == 0 {
            locationField.text = APIConstants.emptyValue
        } else {
            locationField.text = cities?[locationPicker.selectedRow(inComponent: 0)].name ?? APIConstants.emptyValue
            UserDefaults.standard.set(cities?[locationPicker.selectedRow(inComponent: 0)].slug, forKey: APIConstants.citySlug)
        }
        locationField.resignFirstResponder()
    }
    
    @objc func cancelCategory() {
        locationField.resignFirstResponder()
        locationField.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        modalTransitionStyle = .crossDissolve
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        locationField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(restaurantLabel)
        view.addSubview(nameField)
        view.addSubview(locationField)
        view.addSubview(submitButton)
        view.addSubview(progressLabel)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = restaurantLabel.anchor(view.topAnchor, centerX: view.safeAreaLayoutGuide.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels * Constants.safeAreaOffsetMultiplier)
        _ = nameField.anchor(restaurantLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = locationField.anchor(nameField.bottomAnchor, left: nameField.leadingAnchor, right: nameField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = submitButton.anchor(locationField.bottomAnchor, left: locationField.leadingAnchor, right: locationField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = progressLabel.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: -Constants.indexPathDivider, widthConstant: Constants.smallButtonSize, heightConstant: Constants.indicatorCornerRadius)
        
        locationField.addSubview(dropDown)
        _ = dropDown.anchor(right: locationField.trailingAnchor, centerY: locationField.centerYAnchor, rightConstant: Constants.dropDownTrail)
    }
    
    func getCities() {
        let urlString = APIConstants.baseURL + "cities"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(CitiesData.self, from: data!)
                self.cities = [Cities]()
                for dictionary in jsonData.data {
                    let section = Cities(deals: dictionary.deals, slug: dictionary.slug, name: dictionary.name)
                    self.cities?.append(section)
                }
            } catch (let error) {
                print(error)
            }
        }.resume()
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        let name = nameField.text
        let location = UserDefaults.standard.object(forKey: APIConstants.citySlug) as? String ?? APIConstants.emptyValue
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        
        if (name!.isEmpty) {
            nameField.attributedPlaceholder = NSAttributedString(string: StringConstants.restaurantName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (location.isEmpty) {
            locationField.attributedPlaceholder = NSAttributedString(string: StringConstants.location, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                "name": name!,
                "imageUrl": "https://gidieats.com",
                "citySlug": location
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                        APIConstants.authorization: APIConstants.bearer + token
            ]
            
            //Alamofire post request
            AF.request(APIConstants.baseURL + "users/" + userId + "/restaurant", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() {
                    let info = serializeResponse(data: response.data ?? Data()) //Get raw data from backend
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil { //as? NSNull != NSNull.init()
                                let userData = info[APIConstants.data] as! [String: Any]
                                let restaurantSlug = userData["slug"] as? String ?? APIConstants.emptyValue
                                let subaccountCode = UserDefaults.standard.object(forKey: APIConstants.subaccountCode) ?? APIConstants.emptyValue
                                
                                let otherParameters: [String: Any] = [
                                    "subAccountCode": subaccountCode
                                ]
                                
                                let otherHeaders: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                                                 APIConstants.authorization: APIConstants.bearer + token
                                ]
                                
                                AF.request(APIConstants.baseURL + "restaurants/\(restaurantSlug)/sub-account-code", method: .put, parameters: otherParameters, encoding: JSONEncoding.default, headers: otherHeaders).responseJSON { otherResponse in
                                    let otherInfo = serializeResponse(data: otherResponse.data ?? Data())
                                    switch otherResponse.result {
                                    case .success(_):
                                        if otherInfo[APIConstants.isSuccess] as? Bool ?? false == true {
                                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: StringConstants.progressScreen)
                                            self.switchView(value: true)
                                        } else {
                                            let userData = otherInfo[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                                            let message = userData.joined()
                                            self.alert(message: message, title: StringConstants.error)
                                        }
                                    case .failure(_):
                                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                                    }
                                }
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        let response = response.debugDescription
                        if response.contains(APIConstants.unauthorizedResponse) {
                            newToken()
                            
                            let time = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: time) {
                                self.submitForm()
                            }
                        } else if response.contains(APIConstants.badResponse) {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(nil, forKey: APIConstants.savedProfilePicture)
                            
                            self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                        } else {
                            DispatchQueue.main.async {
                                self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                            }
                        }
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
}
