//
//  ForgotPasswordViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/10/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setEmail()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let closeButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.close, for: .normal)
        button.setTitleColor(ColorConstants.gidiGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.closeButtonWeight)
        button.addTarget(self, action: #selector(closePage), for: .touchUpInside)
        return button
    }()
    
    let forgotPasswordLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = StringConstants.forgotPassword
        return label
    }()
    
    let emailField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .emailAddress
        field.returnKeyType = .go
        return field
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        modalTransitionStyle = .crossDissolve
        
        view.backgroundColor = .white
        
        emailField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(closeButton)
        view.addSubview(forgotPasswordLabel)
        view.addSubview(emailField)
        view.addSubview(submitButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = forgotPasswordLabel.anchor(view.topAnchor, centerX: view.safeAreaLayoutGuide.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels * Constants.safeAreaOffsetMultiplier)
        _ = closeButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: Constants.standardBottomConstant)
        _ = emailField.anchor(forgotPasswordLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = submitButton.anchor(emailField.bottomAnchor, left: emailField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        submitForm()
        return true
    }
    
    @objc func submitForm() {
        self.view.endEditing(true)
        
        let email = emailField.text
        
        if (email!.isEmpty) {
            emailField.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (email?.isValidEmail() == false) {
            self.alert(message: StringConstants.invalidEmail, title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                APIConstants.email: email!,
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
            
            AF.request(APIConstants.baseURL + APIConstants.account + APIConstants.forgotPassword, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                UserDefaults.standard.set(email, forKey: APIConstants.emailAddress)
                                
                                self.presentViewController(viewController: ResetPasswordViewController())
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
    
    func setEmail() {
        let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
        self.emailField.text = email
    }
}
