//
//  ResetPasswordViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/10/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.imageAlpha
        return image
    }()
    
    let closeButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.close, for: .normal)
        button.setTitleColor(ColorConstants.gidiGray, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.closeButtonWeight)
        button.addTarget(self, action: #selector(closePage), for: .touchUpInside)
        return button
    }()
    
    let resetPasswordLabel: AuthLabel = {
        let label = AuthLabel()
        label.text = "Reset password"
        return label
    }()
    
    let codeField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.code, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let newPasswordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.newPassword, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .go
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let toggleButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SHOW", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.backgroundColor = ColorConstants.gidiGray.cgColor
        button.layer.cornerRadius = Constants.texFieldHeightOffset
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.regularLabelFont)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.indicatorCornerRadius
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    let resendCodeButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(APIConstants.resendCode, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.addTarget(self, action: #selector(resendCode), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {
        modalTransitionStyle = .crossDissolve
        
        view.backgroundColor = .white
        
        newPasswordField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(closeButton)
        view.addSubview(resetPasswordLabel)
        view.addSubview(codeField)
        view.addSubview(newPasswordField)
        view.addSubview(toggleButton)
        view.addSubview(submitButton)
        view.addSubview(resendCodeButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = resetPasswordLabel.anchor(view.topAnchor, centerX: view.safeAreaLayoutGuide.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels * Constants.safeAreaOffsetMultiplier)
        _ = closeButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, centerX: view.centerXAnchor, bottomConstant: Constants.standardBottomConstant)
        _ = codeField.anchor(resetPasswordLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.textFieldHeight)
        _ = newPasswordField.anchor(codeField.bottomAnchor, left: codeField.leadingAnchor, right: codeField.trailingAnchor, topConstant: Constants.spacingBetweenTextFields, rightConstant: Constants.buttonFormWidth, heightConstant: Constants.textFieldHeight)
        _ = toggleButton.anchor(left: newPasswordField.trailingAnchor, centerY: newPasswordField.centerYAnchor, widthConstant: Constants.buttonFormWidth)
        _ = submitButton.anchor(newPasswordField.bottomAnchor, left: newPasswordField.leadingAnchor, right: codeField.trailingAnchor, topConstant: Constants.buttonToFieldSpacing, heightConstant: Constants.smallButtonSize)
        _ = resendCodeButton.anchor(submitButton.bottomAnchor, centerX: submitButton.centerXAnchor, topConstant: Constants.minConstraintOffset)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    var buttonClick = false
    
    @objc func showPassword() {
        buttonClick = !buttonClick
        if buttonClick == false {
            toggleButton.setTitle("SHOW", for: .normal)
            newPasswordField.isSecureTextEntry = true
        } else {
            toggleButton.setTitle("HIDE", for: .normal)
            newPasswordField.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        submitForm()
        return true
    }
    
    @objc func submitForm() {
        self.view.endEditing(true)
        
        let code = codeField.text
        let password = newPasswordField.text
        let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
        
        if (code!.isEmpty) {
            codeField.attributedPlaceholder = NSAttributedString(string: StringConstants.code, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (password!.isEmpty) {
            newPasswordField.attributedPlaceholder = NSAttributedString(string: StringConstants.newPassword, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (password?.isValidPassword() == false) {
            self.alert(message: StringConstants.shortPassword, title: StringConstants.error)
        } else {
            self.networkLoaderStart()
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorStart()
            
            let parameters: [String: Any] = [
                APIConstants.email: email,
                "passwordResetToken": code!,
                APIConstants.password: password!
            ]
            
            let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
            
            AF.request(APIConstants.baseURL + APIConstants.account + "password/reset", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if Connectivity.isConnected() { //Check connectivity status
                    let info = serializeResponse(data: response.data ?? Data())
                    switch response.result {
                    case .success(_):
                        if info[APIConstants.isSuccess] as? Bool ?? false == true {
                            if info[APIConstants.data] != nil {
                                let userData = info[APIConstants.data] as? String
                                
                                self.alertAndDismissToRoot(message: userData ?? APIConstants.changedPassword, title: StringConstants.success)
                            }
                        } else {
                            let userData = info[APIConstants.errorMessages] as? [String] ?? ["The code inputed is wrong"]
                            let message = userData.joined()
                            self.alert(message: message, title: StringConstants.error)
                        }
                    case .failure(_):
                        self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                    }
                } else {
                    self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
                }
                self.activityIndicatorStop()
                self.view.isUserInteractionEnabled = true
                self.networkLoaderStop()
            }
        }
    }
    
    @objc func resendCode() {
        self.networkLoaderStart()
        
        let email = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue
        let parameters: [String: Any] = [
            APIConstants.email: email,
        ]
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
        
        AF.request(APIConstants.baseURL + APIConstants.account + APIConstants.forgotPassword, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if Connectivity.isConnected() { //Check connectivity status
                let info = serializeResponse(data: response.data ?? Data())
                switch response.result {
                case .success(_):
                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
                        if info[APIConstants.data] != nil {
                            let message = info[APIConstants.data] as? String ?? APIConstants.codeIsResent
                            self.alert(message: message, title: StringConstants.success)
                        }
                    } else {
                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                        let message = userData.joined()
                        self.alert(message: message, title: StringConstants.error)
                    }
                case .failure(_):
                    self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
                }
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
            self.networkLoaderStop()
        }
    }
}
