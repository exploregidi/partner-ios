//
//  TestViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class PaymentAccountViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getBanks()
    }
    
    var banks: [Banks]? = []
    
    let businessName: AuthTextField = {
        let tf = AuthTextField()
        return tf
    }()
    
    let bankName: AuthTextField = {
        let tf = AuthTextField()
        return tf
    }()
    
    let accountNumber: AuthTextField = {
        let tf = AuthTextField()
        return tf
    }()
    
    let submitButton: UIButton = {
        let btn = UIButton()
        btn.layer.backgroundColor = UIColor.green.cgColor
        btn.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        btn.setTitle(StringConstants.submit, for: .normal)
        return btn
    }()
    
    let percentCharge: Float = Constants.percentCharge

    let bankPicker = UIPickerView()
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / Constants.pickerViewDivider))
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return banks?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = banks?[row].name
        return row
    }
    
    @objc func submitForm() {
        let param: [String: Any] = ["business_name": businessName.text!,
                                    "settlement_bank": bankName.text!,
                                    "account_number": accountNumber.text!,
                                    "percentage_charge": percentCharge
        ]
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
                                    APIConstants.authorization: APIConstants.bearer + APIConstants.paystackTestSecretKey
        ]
        
        AF.request(APIConstants.paystackSubaccountUrl, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if Connectivity.isConnected() {
                let info = serializeResponse(data: response.data ?? Data())
                switch response.result {
                case .success(_):
                    let data = info["data"] as! [String: Any]
                    let subaccountCode = data["subaccount_code"]
                    print(subaccountCode!)
                    UserDefaults.standard.set(subaccountCode, forKey: APIConstants.businessSubaccount)
                case .failure(let error):
                    print(error)
                }
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
        }
    }
    
    func getBanks() {
        let urlString = APIConstants.paystackBankListUrl
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(BanksData.self, from: data!)
                for dictionary in jsonData.data {
                    let section = Banks(name: dictionary.name, slug: dictionary.slug, code: dictionary.code, longcode: dictionary.longcode, gateway: dictionary.gateway, payWithBank: dictionary.payWithBank, active: dictionary.active, isDeleted: dictionary.isDeleted, country: dictionary.country, currency: dictionary.currency, type: dictionary.type, id: dictionary.id, createdAt: dictionary.createdAt, updatedAt: dictionary.updatedAt)
                    self.banks?.append(section)
                }
            } catch _ {}
        }.resume()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == bankName {
            showCategoryPicker()
        }
        return true
    }
    
    func showCategoryPicker() {
        bankPicker.dataSource = self
        bankPicker.delegate = self
        bankPicker.showsSelectionIndicator = true
        bankPicker.sizeToFit()
        
        bankName.inputView = bankPicker
        bankName.tintColor = .clear
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.translateAll()
        
        let doneButton = UIBarButtonItem(title: StringConstants.done, style: .plain, target: self, action: #selector(doneCategory))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.cancel, style: .plain, target: self, action: #selector(cancelCategory))
        
        doneButton.tintColor = ColorConstants.gidiGreen
        cancelButton.tintColor = .black
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        bankName.inputAccessoryView = toolbar
    }
    
    @objc func doneCategory() {
        bankName.text = banks?[bankPicker.selectedRow(inComponent: 0)].name
        bankName.resignFirstResponder()
    }
    
    @objc func cancelCategory() {
        bankName.resignFirstResponder()
    }
    
    private func configureUI() {
        view.backgroundColor = .white
        
        bankName.delegate = self
        
        view.addSubview(businessName)
        view.addSubview(bankName)
        view.addSubview(accountNumber)
        view.addSubview(submitButton)
        
        _ = businessName.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, topConstant: 50, leftConstant: 10, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = bankName.anchor(businessName.bottomAnchor, left: businessName.leadingAnchor, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = accountNumber.anchor(bankName.bottomAnchor, left: bankName.leadingAnchor, widthConstant: view.frame.width / Constants.textFielsSpacing - (Constants.leadingConstraintForTextField + Constants.textFielsSpacing), heightConstant: Constants.textFieldHeight)
        _ = submitButton.anchor(accountNumber.bottomAnchor, left: accountNumber.leadingAnchor)
    }
}

/*
 Send subaccount code to devapi to include in the payment parameter //Post
 Get subaccount info and send in parameters for payment //Get and post - final
 Also set api for update subaccount
 */
